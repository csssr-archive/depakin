depakin
=======
<h4>:date: Сроки</h4>
Действие | Дата
--- | ---
:round_pushpin: Вёрстка | 8 - 11 июля<br />
:mount_fuji: Тестирование и отладка | 12 - 13 июля<br />
:checkered_flag: Презентация и финализация | 14 июля

<h4>:link: Макеты</h4> 
_in: https://yadi.sk/d/ll3iVgwRW9FNr
Прототип (описание анимации): https://www.dropbox.com/s/6oozc2rz4tr2o6z/%D0%9F%D1%80%D0%BE%D1%82%D0%BE%D1%82%D0%B8%D0%BF%D1%8B%20%D0%94%D0%B5%D0%BF%D0%B0%D0%BA%D0%B8%D0%BD_Roll-out_01.07.pdf

<h4>:earth_africa: Браузеры</h4>
Только по iPad (Safari,Chrome)

<h4>:triangular_ruler: Ширина страницы</h4>
Фикс под iPad

<h4>:triangular_ruler: Верстается под платформу MI9</h4>
Покурить мануал тут: https://www.dropbox.com/sh/1dzialrolqmyrjx/AAB1vFMqt4wfGxWK0Cl6KLHJa
Аналогично выполненная презентация: https://github.com/CSSSR/Insuman 

<h4>Технические требования</h4>

Поддержка Webkit
Поддержка в частности iOS7 (Safari)
Соответствие техническим требованиям https://www.dropbox.com/s/m0vwwepekf1d7bi/technical.pdf
Каждый слайд должен быть в отдельном sequence
Данная презентация будет объединяться потом с другими презентациями в некую последовательность слайдов для демонстрации, надо это предусмотреть в верстке (необходимо сделать уникальные стили, чтобы по максимуму пресечь возможные пересечения по стилям)
